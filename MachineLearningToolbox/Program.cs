﻿using System;

namespace MachineLearningToolbox.Core
{
    public class Program
    {
        static void Main(string[] args)
        {
            Data.FromResource(@"HW1\SPECT_test.txt",1).each(i => i.Data.Print());

            //as opposed to...
            Data newData = Data.FromFile("..\\..\\Resources\\HW1\\SPECT_test.txt", 1);
            for(int i =0; i < newData.Count; i++)
            {
                Vector datum = newData[i].Data;
                int count;
                if(datum.Cols == 1)
                {
                    count = datum.Rows;
                }
                else
                {
                    count = datum.Cols;
                }
                for(int j = 0; j < count; j++)
                {
                    Console.Write(datum[j] + " ");
                }
                Console.WriteLine();
            }
            }
    }
}
