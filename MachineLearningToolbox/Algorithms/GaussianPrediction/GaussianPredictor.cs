﻿using static MachineLearningToolbox.Core.Math;
using System.Collections.Generic;
using MachineLearningToolbox.Core;

namespace MachineLearningToolbox.Core.Algorithms
{
    public class GaussianPredictor
    { 
        public List<Gaussian> Model { get; set; }
        public GaussianPredictor(Data data, List<double> priors)
        {
            Model = new List<Gaussian>();
            priors.eachIndex(i =>Model.Add(new Gaussian(data.where(d => (int)d.Value == i)).SetPrior(priors[i])));
        }
        public void Classify(Data data)
        {
            data.each(d =>d.Value = max(Model,gauss => gauss.Likelihood(d.Data) * gauss.Prior).Index.AsMatrix().AsVector());
        }
    }
}
