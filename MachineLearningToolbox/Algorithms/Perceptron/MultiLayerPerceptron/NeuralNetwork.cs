﻿using MachineLearningToolbox.Core;
using System.Collections.Generic;

namespace MachineLearningToolbox.Algorithms
{
    public class NeuralNetwork
    {
        protected ITransform OutputLayer { get; set; }
        protected ITransform InputLayer { get; set; }
        protected double LearningRate { get; set; }

        public Stack<Matrix> Evaluate(Matrix m)
        {
            Stack<Matrix> ret = new Stack<Matrix>();
            return Evaluate(m, ret, InputLayer);
        }

        private Stack<Matrix> Evaluate(Matrix currentInputs, Stack<Matrix> ret, ITransform currentLayer)
        {
            ret.Push(currentInputs);
            if (currentLayer.Next == null) return ret;
            Matrix nextInputs = currentLayer.Run(currentInputs);
            return Evaluate(nextInputs, ret, currentLayer.Next);
        }

        public void Backpropagate(Matrix err, Stack<Matrix> states)
        {
            UpdateLearningRate(err);
            states.Pop();
            Backpropagate(err, states, OutputLayer.Prev);
        }

        public void Backpropagate(Matrix err, Stack<Matrix> states, ITransform currentLayer)
        {
            Matrix state = states.Pop();
            if (currentLayer.Prev == null) return;
            Backpropagate(
                err : currentLayer.NextErr(err, state), 
                states : states, 
                currentLayer : currentLayer
                                .Update(err, state, LearningRate)
                                .Prev);
        }
        public virtual void UpdateLearningRate(Matrix err) { }

        
    }
}
