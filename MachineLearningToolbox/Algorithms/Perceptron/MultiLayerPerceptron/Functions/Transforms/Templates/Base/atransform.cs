﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MachineLearningToolbox.Core;

namespace MachineLearningToolbox.Algorithms
{
    public abstract class atransform<Inheritor> : ITransform
    {
        public ITransform Next { get; set; }
        public ITransform Prev { get; set; }

        public Dimension InputDim => inputDim;
        public Dimension OutputDim => outputDim;

        protected Dimension inputDim { get; set; }
        protected Dimension outputDim { get; set; }

        public abstract Inheritor Build(Dimension inputDim, Dimension outputDim);
        

        public Inheritor Update(Matrix error, Matrix input, double learningRate)
        {
            update(error, input, learningRate);
            return (Inheritor)(object)this;
        }
        public abstract Matrix Run(Matrix input);
        public abstract Matrix NextErr(Matrix error, Matrix input);

        protected abstract void update(Matrix error, Matrix input, double learningRate);

        ITransform ITransform.Build(Dimension inputDim, Dimension outputDim)
        {
            return (ITransform)Build(inputDim, outputDim);
        }
        ITransform ITransform.Update(Matrix error, Matrix input, double learningRate)
        {
            return (ITransform)Update(error, input, learningRate);
        }
    }
}
