﻿using MachineLearningToolbox.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MachineLearningToolbox.Algorithms
{
    public interface ITransform
    {
        ITransform Next { get; set; }
        ITransform Prev { get; set; }

        Dimension InputDim { get; }
        Dimension OutputDim { get; }

        ITransform Build(Dimension inputDim, Dimension outputDim);

        ITransform Update(Matrix error, Matrix input, double learningRate);
        Matrix Run(Matrix input);
        Matrix NextErr(Matrix error, Matrix input);
    }
}
