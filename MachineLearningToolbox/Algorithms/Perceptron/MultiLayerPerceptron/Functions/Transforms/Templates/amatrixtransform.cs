﻿using MachineLearningToolbox.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MachineLearningToolbox.Algorithms.Perceptron.MultiLayerPerceptron.Functions.Transforms.Templates
{
    public abstract class amatrixtransform<Inheritor> 
        : atransform<Inheritor>
        where Inheritor : amatrixtransform<Inheritor>, new()
    {
        public Matrix _inner { get; set; }

        protected Func<Matrix, Matrix> transform { get; set; }
        protected Func<Matrix, Matrix> derivative { get; set; }
        public override Inheritor Build(Dimension input, Dimension output)
        {
            //currently assumes input, output vectors are vertical
            Inheritor ret = new Inheritor();
            ret.inputDim = input;
            ret.outputDim = output;
            ret._inner = Matrix.Random(input.Rows, output.Rows);
            return ret;
        }


    }
}
