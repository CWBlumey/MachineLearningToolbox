﻿using MachineLearningToolbox.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MachineLearningToolbox.Algorithms
{
    public class PerceptronModel
    {
        private Vector model { get; set; }
        private double learningRate { get; set; }

        public PerceptronModel(Vector initialModel, double learningRate = .1)
        {
            model = initialModel;
            this.learningRate = learningRate;
        }

        public PerceptronModel(int dimension, double learningRate = .1) : this(Vector.Random(dimension), learningRate) { }

        public double Evaluate(Vector input)
        {
            return (double)(model * input.T);
        }

        public void Backpropagate(Vector input, double error)
        {
            model += -learningRate * error * input;
        }

        public double calculateError(double classification, double correctClass)
        {
            return classification - correctClass;
        }
    }
}
