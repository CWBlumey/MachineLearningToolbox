﻿using System;
using System.Collections.Generic;
using MachineLearningToolbox.Core;

namespace MachineLearningToolbox.Algorithms
{
    public class EM
    {
        public List<Gaussian> Model { get; set; }
        public Data DataSet { get; set; }

        public EM(Data dataSet)
        {
            Model = new List<Gaussian>();
            DataSet = dataSet;
        }

        public EM(Data dataSet, int amount) : this(dataSet)
        {
            for(int i = 0; i < amount; i++)
            {
                Model.Add(
                    new Gaussian(
                        DataSet.where(item =>
                            rand(0,1) > .5)));
            }
        }

        private double rand(int v1, int v2)
        {
            throw new NotImplementedException();
        }

        public void Run(int iterations) //HIGHLY UNOPTIMIZED
        {
            for(int i = 0; i < iterations; i++)
            {
                Model.each(gauss => {
                    Func<Vector, double> weightFunc = v => {
                        double sum = 0;
                        Model.each(cur => { sum += cur.Likelihood(v); });
                        return gauss.Likelihood(v) / sum;
                    };
                    gauss.Avg = Gaussian.Average.WeightFunc(weightFunc)[DataSet];
                    gauss.Cov = Gaussian.Covariance.WeightFunc(weightFunc)[DataSet].AddDiagonal(.01).AsMatrix();});
            }
        }
    }
}
