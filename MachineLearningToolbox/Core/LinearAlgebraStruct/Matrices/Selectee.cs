﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MachineLearningToolbox.Core
{
    public class Selectee<ParentType> : aselect<ParentType> where ParentType : amatrix<ParentType>
    {
        private int minRow { get; set; }
        private int maxRow { get; set; }
        public Selectee(ParentType parent, int minRow, int maxRow)
        {
            this.parent = parent;
            this.minRow = minRow;
            this.maxRow = maxRow;
        }

        public Proxy<ParentType> this[params int[] args]
        {
            get
            {
                Tuple<int, int> mappings = mapInputs(args);
                return new Proxy<ParentType>(parent, minRow, maxRow, mappings.Item1, mappings.Item2);
            }
            set
            {
                Proxy<ParentType> proxy = new Proxy<ParentType>(parent, minRow, maxRow, args[0], args[1]);
                proxy.Set(value);
            }
        }

        public Proxy<ParentType> A
        {
            get
            {
                return this[0, parent.Cols - 1];
            }
            set
            {
                this[0, parent.Cols - 1] = value;
            }
        }

        public Proxy<ParentType> All()
        {
            return A; 
        }
    }
}
