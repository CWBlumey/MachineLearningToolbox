﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MachineLearningToolbox.Core
{
    public abstract class aselect<ParentType> where ParentType : amatrix<ParentType>
    {
        protected ParentType parent { get; set; }
        public ParentType Parent => parent;

        protected Tuple<int, int> mapInputs(int[] args)
        {
            if(args.Length == 1)
            {
                return new Tuple<int, int>(args[0], args[0]);
            }
            else
            {
                return new Tuple<int, int>(args[0], args[1]);
            }
        }
    }
}
