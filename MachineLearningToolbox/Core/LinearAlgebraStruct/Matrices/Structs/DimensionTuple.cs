﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MachineLearningToolbox.Core
{
    public struct DimensionTuple
    {
        public int Dimension1 { get; set; }
        public int Dimension2 { get; set; }
        public DimensionTuple(int dim1, int dim2)
        {
            Dimension1 = dim1;
            Dimension2 = dim2;
        }
    }
}
