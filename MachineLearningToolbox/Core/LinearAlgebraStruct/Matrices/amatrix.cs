﻿using external = MathNet.Numerics.LinearAlgebra;
using System;
using MathNet.Numerics.LinearAlgebra.Double;
using MachineLearningToolbox.Core.Exceptions;

namespace MachineLearningToolbox.Core
{
    public abstract class amatrix<Inheritor> 
        where Inheritor : amatrix<Inheritor> 
    {
        protected static double sparsityThreshhold = .90;
        protected static double toSparseThreshhold = .95;
        protected static double toDenseThreshhold = .85;

        protected external.Matrix<double> inner { get; set; }
        protected double sparsity { get; set; }

        public virtual int Cols { get { return inner.ColumnCount; } }
        public virtual int Rows { get { return inner.RowCount; } }

        public virtual bool IsVector { get { return Cols == 1 || Rows == 1; } }
        public virtual bool Vertical { get { return Cols == 1; } }
        public virtual bool Horizontal { get { return Rows == 1; } }
        public virtual bool IsValue { get { return Rows == 1 && Cols == 1; } }
        public virtual bool IsSquare { get { return Rows == Cols; } }

        public Inheritor I { get { return Inverse(); } }
        public Inheritor Inverse()
        {
            return FromExternalMatrix(innerCopy().Inverse());
        }
        public abstract Inheritor FromExternalMatrix(external.Matrix<double> mat);
        public abstract Inheritor FromArray(double[,] mat);
        public Inheritor T { get { return Transpose(); } }
        public Inheritor Transpose()
        {
            return FromExternalMatrix(innerCopy().Transpose());
        }

        public double Det { get { return Determinant(); } }
        public virtual double Determinant()
        {
            return inner.Determinant();
        }
        public virtual double Magnitude { get { return Determinant(); } }
        public virtual double this[params int[] args]
        {
            get
            {
                Tuple<int, int> mapping = mapInputs(args);
                return inner[mapping.Item1, mapping.Item2];
            }
            set
            {
                Tuple<int, int> mapping = mapInputs(args);
                inner[mapping.Item1, mapping.Item2] = value;
            }
        }
        protected void insertArray(double[,] inner)
        {
            sparsity = GetSparsity(inner);
            if (sparsity > sparsityThreshhold)
            {
                this.inner = SparseMatrix.OfArray(inner);
            }
            else
            {
                this.inner = DenseMatrix.OfArray(inner);
            }
        }
        public static double GetSparsity(double[,] values)
        {
            int sum = 0;
            values.each(i => sum += i == 0 ? 1 : 0);
            return sum / values.Length;
        }
        public static double GetSparsity<T>(amatrix<T> values) where T : amatrix<T>
        {
            int sum = 0;
            values.each(i => sum += i == 0 ? 1 : 0);
            return (double)sum / (double)(values.Cols * values.Rows);
        }
        public double GetSparsity()
        {
            int sum = 0;
            each(i => sum += i == 0 ? 1 : 0);
            return (double)sum / (Cols * Rows);
        }
        public Inheritor AddDiagonal(double val)
        {
            if (!IsSquare)
            {
                throw new DimensionDisagreementException("Matrix must be square to complete this operation");
            }
            for (int i = 0; i < Rows; i++)
            {
                this[i, i] += val;
            }
            return (Inheritor)this;
        }
        protected virtual Tuple<int, int> mapInputs(int[] args)
        {
            if (args.Length == 1)
            {
                if (IsVector)
                {
                    if (Vertical)
                    {
                        return new Tuple<int, int>(args[0], 0);
                    }
                    else
                    {
                        return new Tuple<int, int>(0, args[0]);
                    }
                }
                else
                {
                    throw new ArgumentException("At least 2 arguments are necessary to index a matrix.");
                }
            }
            else
            {
                return new Tuple<int, int>(args[0], args[1]);
            }
        }

        public virtual Selector<Inheritor> S { get { return new Selector<Inheritor>((Inheritor)this); } }
        public virtual Selectee<Inheritor> SA { get { return new Selectee<Inheritor>((Inheritor)this, 0, this.Rows - 1); } }

        
        public virtual Inheritor Copy()
        {
            double[,] copy = new double[Rows, Cols];
            copy.eachIndex((i, j) => copy[i, j] = this[i, j]);
            return FromArray(copy);
        }

        public virtual Inheritor eachIndex(Action<int,int> func, Func<int, int, bool> terminate)
        {
            for(int i = 0; i < Rows; i++)
            {
                for(int j = 0; j < Cols; j++)
                {
                    func(i, j);
                    if (terminate(i, j)) return (Inheritor)this;
                }
            }
            return (Inheritor)this;
        }
        public virtual Inheritor each(Action<double> func, Func<double,bool> terminate)
        {
            return eachIndex(
                (i, j) => func(this[i, j]),
                (i, j) => terminate(this[i, j]));
        }
        public virtual Inheritor eachIndex(Action<int, int> func)
        {
            return eachIndex(
                func,
                (i, j) => false);
        }
        public virtual Inheritor each(Action<double> func)
        {
            return each(
                func,
                i => false);
        }
        public virtual Inheritor eachIndex(Action<double> func, Func<bool> terminate)
        {
            return eachIndex(
                (i, j) => func(this[i, j]),
                (i, j) => terminate());
        }
        public virtual Inheritor each(Action<int, int> func, Func<bool> terminate)
        {
            return eachIndex(
                func,
                (i, j) => terminate());
        }
        protected virtual MathNet.Numerics.LinearAlgebra.Matrix<double> innerCopy()
        {
            double[,] copy = new double[Rows, Cols];
            for (int i = 0; i < Rows; i++)
            {
                for (int j = 0; j < Cols; j++)
                {
                    copy[i, j] = this[i, j];
                }
            }
            double sparsity = GetSparsity(copy);
            if (sparsity > sparsityThreshhold)
            {
                return MathNet.Numerics.LinearAlgebra.Double.SparseMatrix.OfArray(copy);
            }
            return MathNet.Numerics.LinearAlgebra.Double.DenseMatrix.OfArray(copy);
        }
        #region overloads
        #region *
        public static Matrix operator *(Matrix a, amatrix<Inheritor> b)
        {
            return new Matrix(
                a.innerCopy()
                .Multiply(
                    b.innerCopy()));
        }
        #endregion

        public static Matrix operator /(Matrix a, amatrix<Inheritor> b)
        {
            return new Matrix(
                a.innerCopy()
                .Multiply(
                    b.innerCopy()
                    .Inverse()));
        }

        public static Inheritor operator +(amatrix<Inheritor> a, amatrix<Inheritor> b)
        {
            return a.FromExternalMatrix(
                a.innerCopy()
                .Add(
                    b.innerCopy()));
        }

        public static Inheritor operator -(amatrix<Inheritor> a, amatrix<Inheritor> b)
        {
            return a.FromExternalMatrix(
                a.innerCopy()
                .Subtract(
                    b.innerCopy()));
        }

        public static Inheritor operator -(amatrix<Inheritor> a)
        {
            var acopy = a.innerCopy();
            return a
                .eachIndex(
                (i, j) => acopy[i, j] = -acopy[i, j])
                .FromExternalMatrix(acopy);
        }

        public static Inheritor operator *(double d, amatrix<Inheritor> a)
        {
            var acopy = a.innerCopy();
            return a.FromExternalMatrix(
                a.innerCopy()
                .Multiply(d));
        }
        public static Inheritor operator *(amatrix<Inheritor> a, double d)
        {
            return d * a;
        }

        public static Inheritor operator /(amatrix<Inheritor> a, double d)
        {
            return a.FromExternalMatrix(
                a.innerCopy()
                .Multiply(d));
        }
        
        public static Inheritor operator +(amatrix<Inheritor> a, double d)
        {
            return a.FromExternalMatrix(
                a.innerCopy()
                .Add(d));
        }
        public static Inheritor operator +(double d, amatrix<Inheritor> a)
        {
            return a + d;
        }

        public static Inheritor operator -(amatrix<Inheritor> a, double d)
        {
            return a.FromExternalMatrix(
                a.innerCopy()
                .Subtract(d));
        }
        public static amatrix<Inheritor> operator -(double d, amatrix<Inheritor> a)
        {
            return a - d;
        }
        public static Inheritor operator *(amatrix<Inheritor> a, int i)
        {
            return a * (double)i;
        }

        public static amatrix<Inheritor> operator *(int i, amatrix<Inheritor> a)
        {
            return a * (double)i;
        }

        public static Inheritor operator /(amatrix<Inheritor> a, int i)
        {
            return a / (double)i;
        }
        public static Inheritor operator +(amatrix<Inheritor> a, int i)
        {
            return a + (double)i;
        }
        public static Inheritor operator +(int i, amatrix<Inheritor> a)
        {
            return a + (double)i;
        }
        public static Inheritor operator -(amatrix<Inheritor> a, int i)
        {
            return a - (double)i;
        }
        public static Inheritor operator -(int i, amatrix<Inheritor> a)
        {
            return a - (double)i;
        }
        #endregion
        public static explicit operator double(amatrix<Inheritor> a)
        {
            if (a.IsValue)
            {
                return a[0, 0];
            }
            throw new DimensionDisagreementException("Cannot convert non-trivial matrix to value");
        }
        public static implicit operator Matrix(amatrix<Inheritor> a)
        {
            return a.AsMatrix();
        }

        public void ByRow(Action<Proxy<Inheritor>> func, Func<Proxy<Inheritor>,bool> terminate)
        {
            for(int i = 0; i < Rows; i++)
            {
                var proxy = S[i].A;
                func(proxy);
                if (terminate(proxy)) return;
            }
        }
        public Vector AsVector()
        {
            return new Vector(inner);
        }
        public Matrix AsMatrix()
        {
            return new Matrix(inner);
        }
        public void ByRow(Action<Proxy<Inheritor>> func, Func<bool> terminate)
        {
            ByRow(
                func,
                a => terminate());
        }
        public void ByRow(Action<Proxy<Inheritor>> func)
        {
            ByRow(func, i => false);
        }
        public void ByCol(Action<Proxy<Inheritor>> func, Func<Proxy<Inheritor>, bool> terminate)
        {
            for(int i = 0; i < Cols; i++)
            {
                var proxy = SA[i];
                func(proxy);
                if (terminate(proxy)) return;
            }
        }
        public void ByCol(Action<Proxy<Inheritor>> func, Func<bool> terminate)
        {
            ByCol(func, i => terminate());
        }
        public void ByCol(Action<Proxy<Inheritor>> func)
        {
            ByCol(func, i => false);
        }

        public void Print()
        {
            ByRow(row =>
                {
                    row.ByCol(val => Console.Write((double)val + " "));
                    Console.WriteLine();
                });
        }

        public Dimension GetDimension()
        {
            return new Dimension(Rows, Cols);
        }
    }
}
