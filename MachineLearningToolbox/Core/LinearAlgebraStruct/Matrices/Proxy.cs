﻿using System;
using MathNet.Numerics.LinearAlgebra;
using MachineLearningToolbox.Core.Exceptions;

namespace MachineLearningToolbox.Core
{
    public class Proxy<ParentType> 
        : amatrix<Proxy<ParentType>> 
        where ParentType : amatrix<ParentType>
    {
        private int maxCol;
        private int minCol;
        private ParentType parent;
        private int minRow;
        private int maxRow;
        public override int Cols
        {
            get
            {
                return maxCol - minCol + 1;
            }
        }
        public override int Rows
        {
            get
            {
                return maxRow - minRow + 1;
            }
        }

        public Proxy(ParentType parent, int minRow, int maxRow, int minCol, int maxCol) : base()
        {
            this.parent = parent;
            this.minRow = minRow;
            this.maxRow = maxRow;
            this.minCol = minCol;
            this.maxCol = maxCol;
        }
        public override double this[params int[] args]
        {
            get
            {
                Tuple<int, int> mapping = mapInputs(args);
                return parent[mapping.Item1, mapping.Item2];
            }
            set
            {
                Tuple<int, int> mapping = mapInputs(args);
                parent[mapping.Item1, mapping.Item2] = value;
            }
        }
        protected override Tuple<int,int> mapInputs(int[] args)
        {
            if(IsVector && args.Length == 1)
            {
                if (Vertical)
                {
                    return new Tuple<int, int>(args[0] + minRow, 0 + minCol);
                }
                else
                {
                    return new Tuple<int, int>(0 + minRow, args[0] + minCol);
                }
            }
            else
            {
                return new Tuple<int, int>(args[0] + minRow, args[1] + minCol);
            }
        }
        public void Set<T>(amatrix<T> setter) where T : amatrix<T>
        {
            if (setter.Cols != Cols || setter.Rows != Rows) throw new DimensionDisagreementException("Matrix dimensions must agree");
            setter.eachIndex(
                (i, j) => this[i, j] = setter[i, j]);
        }
        public void Set(double[,] setter)
        {
            if (setter.GetLength(1) != Cols || setter.GetLength(0) != Rows) throw new DimensionDisagreementException("Matrix dimensions must agree");
            for (int i = 0; i < Rows; i++)
            {
                for (int j = 0; j < Cols; j++)
                {
                    this[i, j] = setter[i, j];
                }
            }
        }

        public override Proxy<ParentType> FromExternalMatrix(Matrix<double> mat)
        {
            return new Proxy<ParentType>(parent.FromExternalMatrix(mat), 0, mat.RowCount - 1, 0, mat.ColumnCount - 1);
        }

        public override Proxy<ParentType> FromArray(double[,] mat)
        {
            return new Proxy<ParentType>(parent.FromArray(mat), 0, mat.GetLength(0)-1, 0, mat.GetLength(1)-1);
        }
    }
}