﻿using System;
using MathNet.Numerics.LinearAlgebra;
using MachineLearningToolbox.Core.Exceptions;
using static System.Math;
using static MachineLearningToolbox.Core.Math;

namespace MachineLearningToolbox.Core
{
    public class Vector : amatrix<Vector>
    {
        public Vector(double[] inner)
        {
            insertArray(inner.To2DArray());
        }
        public Vector(Matrix<double> inner)
        {
            if(inner.ColumnCount != 1 && inner.RowCount != 1)
            {
                throw new DimensionDisagreementException("Matrix must be 1D");
            }
            this.inner = inner;
        }

        public override double Magnitude
        {
            get
            {
                double sum = 0;
                eachIndex(i => sum += Pow(this[i], 2));
                return Sqrt(sum);
            }
        }
        public int Dim { get { if (Vertical) return Rows; return Cols; } }
        public Vector eachIndex(Action<int> func, Func<int,bool> terminate)
        {
            int to = Vertical ? Rows : Cols;
            for(int i = 0; i < to; i++)
            {
                func(i);
                if (terminate(i)) return this;
            }
            return this;
        }
        public Vector eachIndex(Action<int> func, Func<bool> terminate)
        {
            return eachIndex(
                func,
                i => terminate());
        }
        public Vector eachIndex(Action<int> func)
        {
            return eachIndex(
                func,
                i => false);
        }
        public override double Determinant()
        {
            throw new DimensionDisagreementException("Cannot take the determinant of a vector");
        }

        public override Vector FromExternalMatrix(Matrix<double> mat)
        {
            return new Vector(mat);
        }

        public override Vector FromArray(double[,] mat)
        {
            if (!(mat.GetLength(0) == 0 || mat.GetLength(1) == 0)) throw new DimensionDisagreementException("Cannot create vector from 2D array");
            return new Vector(mat.To1DArray());
        }

        public int Dimension { get
            {
                return Rows == 1 ? Cols : Rows;
            } }

        public static Vector Zeroes(int n)
        {
            return new Vector(new double[n]);
        }

        public static Vector Random(int dimension)
        {
            double[,] inner = new double[1, dimension];
            return new Matrix(inner.eachIndex((i, j) => inner[i, j] = rand(-1, 1))).AsVector();
        }
    }
}
