﻿using external = MathNet.Numerics.LinearAlgebra;
using static MachineLearningToolbox.Core.Math;

namespace MachineLearningToolbox.Core
{
    public class Matrix : amatrix<Matrix>
    {
        public Matrix(external.Matrix<double> inner) : base()
        {
            this.inner = inner;
            sparsity = GetSparsity(this);
        }
        public Matrix(double[,] inner) : base()
        {
            insertArray(inner);
        }
        public Matrix(double[] inner)
        {
            double[,] ret = new double[inner.Length, inner.Length];
            for(int i = 0; i < inner.Length; i++)
            {
                ret[i,i] = inner[i];
            }
            insertArray(ret);
        }
        public static Matrix Zeroes(int dimensions)
        {
            return Zeroes(dimensions, dimensions);
        }
        public static Matrix Zeroes(int dimensions1, int dimensions2)
        {
            return new Matrix(new double[dimensions1, dimensions2]);
        }

        public static Matrix Diagonal(double vals, int dimensions)
        {
            double[] ret = new double[dimensions];
            for(int i = 0; i < ret.Length; i++)
            {
                ret[i] = vals;
            }
            return Diagonal(ret);
        }
        public static Matrix Diagonal(double[] vals)
        {
            return new Matrix(vals);
        }

        public override Matrix FromExternalMatrix(external.Matrix<double> mat)
        {
            return new Matrix(mat);
        }

        public override Matrix FromArray(double[,] mat)
        {
            return new Matrix(mat);
        }

        public static Matrix Random(int rows, int cols)
        {
            double[,] inner = new double[rows, cols];
            return new Matrix(
                inner.eachIndex((i, j) =>
                inner[i, j] = rand(-1, 1)
                ));
        }
    }
}
