﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MachineLearningToolbox.Core
{
    public class Selector<ParentType> 
        : aselect<ParentType> 
        where ParentType : amatrix<ParentType>
    {
        public Selector(ParentType parent)
        {
            this.parent = parent;
        }

        public Selectee<ParentType> this[params int[] args]
        {
            get
            {
                Tuple<int, int> inputs = mapInputs(args);
                return new Selectee<ParentType>(parent, inputs.Item1, inputs.Item2);
            }
        }

        public Selectee<ParentType> A
        {
            get
            {
                return this[0, parent.Rows-1]; 
            }
        }

        public Selectee<ParentType> All()
        {
            return A;
        }
    }
}
