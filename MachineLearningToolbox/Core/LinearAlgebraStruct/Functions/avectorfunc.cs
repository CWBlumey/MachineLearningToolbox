﻿using System;

namespace MachineLearningToolbox.Core
{
    public abstract class avectorfunc
    {
        public Func<double, double> _eachFunc    { get; set; } = i => i;
        public bool _preserveOriginal           { get; set; } = false;
        public Vector _innerValue               { get; set; } = 0.AsVector();

        public avectorfunc OperateOn(double val)
        {
            _innerValue = val.AsVector();
            return this;
        }

        public avectorfunc OperateOn(Vector val)
        {
            _innerValue = val;
            return this;
        }

        public avectorfunc PreserveOriginal(bool preserve = true)
        {
            _preserveOriginal = preserve;
            return this;
        }

        public avectorfunc EachFunc(Func<double, double> func)
        {
            _eachFunc = func;
            return this;
        }

        public Vector this[double val]
        {
            get
            {
                _innerValue = val.AsVector();
                return Invoke();
            }
        }

        public Vector this[Vector val]
        {
            get
            {
                _innerValue = val;
                return Invoke();
            }
        }

        public Vector Invoke()
        {
            var a = _preserveOriginal ? _innerValue.Copy() : _innerValue;
            return a.eachIndex(i => a[i] = _eachFunc(a[i]));
        }
    }
}
