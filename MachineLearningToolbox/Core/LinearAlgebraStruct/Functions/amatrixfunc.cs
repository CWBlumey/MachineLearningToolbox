﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MachineLearningToolbox.Core
{
    public abstract class amatrixfunc
    {
        protected abstract Func<double,double> eachFunc { get; set; }
        public double this[double value]
        {
            get
            {
                return eachFunc(value);
            }
        }

        public Matrix this[Matrix value, bool copy = true]
        {
            get
            {
                Matrix m = copy ? value.Copy() : value;
                return m
                    .eachIndex
                    ((i, j) => m[i, j] = this[m[i, j]]);
            }
        }

        public double Run(double value)
        {
            return this[value];
        }

        public Matrix Run(Matrix value, bool copy = true)
        {
            return this[value, copy];
        }

    }
}
