﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MachineLearningToolbox.Core
{
    public class AverageBuilder
    {
        public Func<Vector, Vector, double> _weightFunc     { get; set; } = (i, j) => 1;
        public double _initialAmount                        { get; set; } = 0;
        public Vector _initialAverage                       { get; set; } = null;
        public Data _inputData                              { get; set; } = null;
        public double _finalAmount                          { get; set; } = -1;
        public double _finalAverage                         { get; set;}

        public AverageBuilder WeightFunc(Func<Vector, Vector, double> weightFunc)
        {
            _weightFunc = weightFunc;
            return this;
        }
        public AverageBuilder WeightFunc(Func<Vector, double> weightFunc)
        {
            return WeightFunc((i, j) => weightFunc(i));
        }
        public AverageBuilder InitialAmount(double previousAmount)
        {
            _initialAmount = previousAmount;
            return this;
        }

        public AverageBuilder InputData(Data data)
        {
            _inputData = data;
            return this;
        }

        public AverageBuilder InitialAverage(Vector initial)
        {
            _initialAverage = initial;
            return this;
        }

        public Vector this[Data data]
        {
            get { _inputData = data; if (_initialAverage == null || _initialAverage.Dimension != data.Dimension) _initialAverage = Vector.Zeroes(data.Dimension); return this.Invoke(); }
        }

        public Vector Invoke()
        {
            errorCheck();

            var amt = _initialAmount;
            var sum = _initialAverage * _initialAmount;
            _inputData.each(i => 
                {
                    double weight = _weightFunc(i.Data, sum);
                    amt += weight;
                    sum += weight * i.Data;
                });
            return sum / amt;
        }

        private void errorCheck()
        {
            if (_inputData == null || (_initialAverage != null && _initialAverage.Dimension != _inputData.Dimension)) throw new Exception("");
            if (_initialAverage == null) _initialAverage = Vector.Zeroes(_inputData.Dimension);
        }
    }
}
