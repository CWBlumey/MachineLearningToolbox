﻿using System;

namespace MachineLearningToolbox.Core
{
    public class CovarianceBuilder
    {
        public Func<Vector, Matrix, Vector, double> _weightFunc     { get; set; } = (i, j, k) => 1;
        public AverageBuilder _averageFunc                          { get; set; } = new AverageBuilder();
        public Vector _average                                      { get; set; } = null;
        public Matrix _initialCovariance                            { get; set; } = null;
        public double _initialAmount                                { get; set; } = 0;
        public Data _data                                           { get; set; } = null;
        
        public CovarianceBuilder WeightFunc(Func<Vector, Matrix, Vector, double> weightFunc)
        {
            _weightFunc = weightFunc;
            return this;
        }

        public CovarianceBuilder WeightFunc(Func<Vector, Matrix, double> weightFunc)
        {
            return WeightFunc(
                (i, j, k) => weightFunc(i, j));
        }

        public CovarianceBuilder WeightFunc(Func<Vector, double> weightFunc)
        {
            return WeightFunc((i, j) => weightFunc(i));
        }

        public CovarianceBuilder Average(Vector average)
        {
            _average = average;
            return this;
        }

        public CovarianceBuilder InitialCovariance(Matrix covariance)
        {
            _initialCovariance = covariance;
            return this;
        }
        
        public CovarianceBuilder InitialAmount(double amount)
        {
            _initialAmount = amount;
            _averageFunc.InitialAmount(amount);
            return this;
        }

        public CovarianceBuilder GlobalWeightFunc(Func<Vector, double> weightFunc)
        {
            _averageFunc.WeightFunc(weightFunc);
            return WeightFunc(weightFunc);
        }

        public CovarianceBuilder Data(Data data)
        {
            if (_average == null) _average = Vector.Zeroes(data.Dimension);
            if (_initialCovariance == null) _initialCovariance = Matrix.Zeroes(data.Dimension);
            _data = data;
            return this;
        }

        public Matrix this[Data d]
        {
            get
            {
                Data(d);
                return Invoke();
            }
        }

        public Matrix Invoke()
        {
            errorCheck();

            Vector avg = _average ?? _averageFunc[_data];
            Matrix cov = _initialCovariance ?? Matrix.Zeroes(_data.Dimension);
            double amt = _initialAmount;

            _data.each(i =>
            {
                double weight = _weightFunc(i.Data, cov, avg);
                amt += weight;
                Vector offset = i.Data.T - avg;
                cov += weight * (offset.T * offset);
            });

            return cov / amt;
        }

        private void errorCheck()
        {
            if (_data == null ||
                (_average.Dimension != _data.Dimension) ||
                (_initialCovariance.GetDimension().Cols != _data.Dimension) ||
                !_initialCovariance.IsSquare) throw new Exception();
        }
    }
}
