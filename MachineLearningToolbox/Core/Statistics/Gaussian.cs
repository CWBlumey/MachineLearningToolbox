﻿using static System.Math;

namespace MachineLearningToolbox.Core
{
    public class Gaussian
    {
        public Vector Avg { get; set; }
        public Matrix Cov { get; set; }
        public double Prior { get; set; } = -1;
        public int? Class { get; set; } = -1;

        public Gaussian(Vector average, Matrix covariance)
        {
            Avg = average;
            Cov = covariance;
        }
        
        public Gaussian(Data data)
        {
            Avg = Average[data];
            Cov = Covariance.Average(Avg)[data];
        }

        public double Likelihood(Vector v)
        {
            Vector offset = (v - Avg).AsVector();
            double exp = Exp((double)(offset * Cov * offset.T));
            var d = Cov.Determinant();
            double denom = 2 * PI * Pow(Cov.Determinant(),.5);
            return exp / denom;
        }
        public Gaussian SetPrior(double prior)
        {
            Prior = prior;
            return this;
        }
        public Gaussian SetClass(int classVal)
        {
            Class = classVal;
            return this;
        }
        public static AverageBuilder Average { get { return new AverageBuilder();  } }
        public static CovarianceBuilder Covariance { get { return new CovarianceBuilder();  } }
    }
}
