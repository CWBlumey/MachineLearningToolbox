﻿namespace MachineLearningToolbox.Core
{
    public static class Moment
    {
        public static AverageBuilder Average { get { return new AverageBuilder();  } }
        public static CovarianceBuilder Covariance { get { return new CovarianceBuilder(); } }
    }
}
