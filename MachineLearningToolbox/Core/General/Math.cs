﻿using static System.Math;

namespace MachineLearningToolbox.Core.Mathematics
{
    public static class Math
    {
        public static double Sigmoid(double value)
        {
            return 1 / (1 + (Pow(E, -value)));
        }
    }
}
