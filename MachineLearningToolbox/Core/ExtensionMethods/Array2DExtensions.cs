﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MachineLearningToolbox.Core
{
    public static class Array2DExtensions
    {
        public static T[] To1DArray<T>(this T[,] array)
        {
            int row = array.GetLength(0);
            int col = array.GetLength(1);
            T[] ret = new T[col * row];
            array.eachIndex((i, j) => ret[(i * col) + j] = array[i, j]);
            return ret;
        }

        public static T[,] eachIndex<T>(this T[,] array, Action<int,int> func, Func<int, int, bool> terminate)
        {
            for(int i = 0; i < array.GetLength(0); i++)
            {
                for(int j = 0; j < array.GetLength(1); j++)
                {
                    func(i,j);
                    if (terminate(i,j)) return array;
                }
            }
            return array;
        }
        public static T[,] each<T>(this T[,] array, Action<T> func, Func<int,int, bool> terminate)
        {
            return array.eachIndex((i,j) => func(array[i,j]),(i,j) => terminate(i,j));
        }
        public static T[,] eachIndex<T>(this T[,] array, Action<int,int> func, Func<bool> terminate)
        {
            return array.eachIndex(
                (i, j) => func(i, j),
                (i, j) => terminate());
        }
        public static T[,] each<T>(this T[,] array, Action<T> func, Func<bool> terminate)
        {
            return array.eachIndex(
                (i, j) => func(array[i, j]),
                (i, j) => terminate());
        }
        public static T[,] eachIndex<T>(this T[,] array, Action<int, int> func)
        {
            return array.eachIndex(
                (i, j) => func(i, j),
                (i, j) => false);
        }
        public static T[,] each<T>(this T[,] array, Action<T> func)
        {
            return array.eachIndex(
                (i, j) => func(array[i, j]),
                (i, j) => false);
        }
    }
}
