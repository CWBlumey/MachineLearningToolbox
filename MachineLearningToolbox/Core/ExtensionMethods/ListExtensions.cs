﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MachineLearningToolbox.Core
{
    public static class ListExtensions
    {
        public static List<T> eachIndex<T>(this List<T> list, Action<int> func, Func<int, bool> terminate)
        {
            for(int i = 0; i < list.Count; i++)
            {
                func(i);
                if (terminate(i)) return list;
            }
            return list;
        }
        public static List<T> each<T>(this List<T> list, Action<T> func, Func<T,bool> terminate)
        {
            return list.eachIndex(
                i => func(list[i]), 
                i => terminate(list[i]));
        }
        public static List<T> eachIndex<T>(this List<T> list, Action<int> func, Func<bool> terminate)
        {
            return list.eachIndex(
                func,
                i => terminate());
        }
        public static List<T> each<T>(this List<T> list, Action<T> func, Func<bool> terminate)
        {
            return list.eachIndex(
                i => func(list[i]),
                i => terminate());
        }
        public static List<T> eachIndex<T>(this List<T> list, Action<int> func)
        {
            return list.eachIndex(
                func,
                i => false);
        }
        public static List<T> each<T>(this List<T> list, Action<T> func)
        {
            return list.eachIndex(
                i => func(list[i]),
                i => false);
        }
        public static List<T> PopLast<T>(this List<T> list, int n)
        {
            List<T> ret = new List<T>(new T[n]);
            for(int i = 0; i < n; i++)
            {
                ret[i] = list[ list.Count + i - n];
            }
            list.RemoveRange(list.Count - n, n);
            return ret;
        }
        public static List<T> PopFirst<T>(this List<T> list, int n)
        {
            List<T> ret = new List<T>(new T[n]);
            for (int i = 0; i < n; i++)
            {
                ret[i] = list[i];
            }
            list.RemoveRange(0, n);
            return ret;
        }
    }
}
