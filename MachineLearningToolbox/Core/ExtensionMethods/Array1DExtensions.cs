﻿using System;

namespace MachineLearningToolbox.Core
{
    public static class Array1DExtensions
    {
        public static T[,] To2DArray<T>(this T[] array)
        {
            T[,] ret = new T[1, array.Length];
            array.eachIndex(i => ret[0, i] = array[i]);
            return ret;
        }

        public static T[] eachIndex<T>(this T[] array, Action<int> func, Func<int, bool> terminate)
        {
            for(int i = 0; i < array.Length; i++)
            {
                func(i);
                if (terminate(i)) return array;
            }
            return array;
        }
        public static T[] each<T>(this T[] array, Action<T> func, Func<T, bool> terminate)
        {
            return array.eachIndex(
                i => func(array[i]), 
                i => terminate(array[i]));
        }
        public static T[] eachIndex<T>(this T[] array, Action<int> func)
        {
            return array.eachIndex(
                i => func(i),
                i => false);
        }
        public static T[] each<T>(this T[] array, Action<T> func)
        {
            return array.eachIndex(
                i => func(array[i]),
                i => false);
        }
        public static T[] eachIndex<T>(this T[] array, Action<int> func, Func<bool> terminate)
        {
            return array.eachIndex(func, i => terminate());
        }
        public static T[] each<T>(this T[] array, Action<T> func, Func<bool> terminate)
        {
            return array.eachIndex(i => func(array[i]), i => terminate());
        }
    }
}
