﻿using static System.Math;

namespace MachineLearningToolbox.Core
{
    public static class IntegerExtensions
    {
        public static Vector AsBinary(this int value, int digits)
        {
            Vector v = new Vector(new double[digits]);
            int x = value % (int)Pow(2,digits);
            v.eachIndex
                (i =>
                    {
                        double baseAmt = Pow(2, digits - i-1);
                        v[i] = Floor(x != 0 ? (x != 1 ? Log(x, baseAmt) : (baseAmt == 1 ? 1 : 0)) : 0);
                        x %= (int)baseAmt;
                    }
                );
            return v;
        }
        public static Vector AsBinary(this int value)
        {
            if(value == 0 || value == 1)
            {
                return value.AsBinary(1);
            }
            return value.AsBinary((int)Floor(Log(value, 2)) + 1);
        }
        public static Matrix AsMatrix(this int value)
        {
            return new Matrix(new double[1] { value });
        }
        public static Vector AsVector(this int value)
        {
            return new Vector(new double[1] { value });
        }
    }
}
