﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MachineLearningToolbox.Core
{
    public static class DictionaryExtensions
    {
        public static Dictionary<T2,T1> Reverse<T1,T2>(this Dictionary<T1,T2> original)
        {
            Dictionary<T2, T1> ret = new Dictionary<T2, T1>();
            foreach(var i in original)
            {
                ret.Add(i.Value, i.Key);
            }
            return ret;
        }
    }
}
