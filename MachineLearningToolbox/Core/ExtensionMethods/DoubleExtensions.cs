﻿namespace MachineLearningToolbox.Core
{
    public static class DoubleExtensions
    {
        public static Matrix AsMatrix(this double value)
        {
            return new Matrix(new double[1] { value });
        }

        public static Vector AsVector(this double value)
        {
            return new Vector(new double[1] { value });
        }
    }
}
