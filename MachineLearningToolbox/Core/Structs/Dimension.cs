﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MachineLearningToolbox.Core
{
    public struct Dimension
    {
        public int Rows { get; set; }
        public int Cols { get; set; }

        public Dimension(int rows, int cols)
        {
            Rows = rows;
            Cols = cols;
        }
    }
}
