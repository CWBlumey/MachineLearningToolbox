﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MachineLearningToolbox.Core.Exceptions
{
    public class DimensionDisagreementException : Exception
    {
        public DimensionDisagreementException() : base() { }
        public DimensionDisagreementException(string message) : base(message) { }
        public DimensionDisagreementException(string message, Exception inner) : base(message, inner) { } 
    }
}
