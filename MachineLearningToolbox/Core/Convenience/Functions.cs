﻿using System;

namespace MachineLearningToolbox.Core
{
    public static class Functions
    {
        public static void print(double d, bool newLine = true)
        {
            if (newLine)
            {
                Console.WriteLine(d);
            }
            else
            {
                Console.Write(d);
            }
        }
        public static void print(int d, bool newLine = true)
        {
            if (newLine)
            {
                Console.WriteLine(d);
            }
            else
            {
                Console.Write(d);
            }
        }
        public static void print(string d, bool newLine = true)
        {
            if (newLine)
            {
                Console.WriteLine(d);
            }
            else
            {
                Console.Write(d);
            }
        }
        public static void print(char d, bool newLine = true)
        {
            if (newLine)
            {
                Console.WriteLine(d);
            }
            else
            {
                Console.Write(d);
            }
        }
        public static void print(bool d, bool newLine = true)
        {
            if (newLine)
            {
                Console.WriteLine(d);
            }
            else
            {
                Console.Write(d);
            }
        }
        public static void print(object d, bool newLine = true)
        {
            if (newLine)
            {
                Console.WriteLine(d);
            }
            else
            {
                Console.Write(d);
            }
        }
        public static void print<T>(amatrix<T> a) where T : amatrix<T>
        {
            a.Print();
        }
        public static string read()
        {
            return Console.ReadLine();
        }

        
    }
}
