﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MachineLearningToolbox.Core
{
    public class MinMaxResult<T>
    {
        public T Item { get; set; }
        public double Score { get; set; }
        public int Index { get; set; }
        public MinMaxResult<T> Next { get; set; }
        public MinMaxResult(T item, double score, int index)
        {
            Index = index;
            Score = score;
            Item = item;
        }
        public List<MinMaxResult<T>> ToList()
        {
            MinMaxResult<T> cur = this;
            List<MinMaxResult<T>> ret = new List<MinMaxResult<T>>();
            ret.Add(cur);
            while(cur.Next != null)
            {
                cur = cur.Next;
                ret.Add(cur);
            }
            return ret;
        }
    }
}
