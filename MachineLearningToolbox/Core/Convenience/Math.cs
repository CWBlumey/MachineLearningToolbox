﻿using System;
using System.Collections.Generic;

namespace MachineLearningToolbox.Core
{
    public static class Math
    {
        public static MinMaxResult<T> max<T>(List<T> list, Func<T,double> heuristic)
        {
            MinMaxResult<T> baseRet = new MinMaxResult<T>(default(T), double.NaN, -1);
            MinMaxResult<T> lastRet = baseRet;
            list.eachIndex(i =>
            {
                T item = list[i];
                double score = heuristic(item);
                if(score >= baseRet.Score || baseRet.Score == double.NaN)
                {
                    if(score > baseRet.Score || baseRet.Score == double.NaN)
                    {
                        baseRet = new MinMaxResult<T>(item, score, i);
                        lastRet = baseRet;
                    }
                    else
                    {
                        MinMaxResult<T> newLast = new MinMaxResult<T>(item, score, i);
                        lastRet.Next = newLast;
                        lastRet = newLast;
                    }
                }
            });
            return baseRet;
        }
        public static MinMaxResult<T> min<T>(List<T> list, Func<T,double> heuristic)
        {
            MinMaxResult<T> ret = max(list, i => -heuristic(i));
            ret.ToList().each(item => item.Score = -item.Score);
            return ret;
        }
        private static Random _rand = new Random();
        public static double rand(double min, double max)
        {
            return min + (_rand.NextDouble() * (max - min));
        }

        public static int RandInt(int min, int max)
        {
            return _rand.Next(min, max);
        }
    }
}
