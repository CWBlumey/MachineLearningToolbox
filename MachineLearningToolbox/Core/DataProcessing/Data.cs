﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace MachineLearningToolbox.Core
{
    public class Data
    {
        public int Count { get { return data.Count; } }
        public int Dimension => data[0].Data.Dimension; 

        public Datum this[int index]
        {
            get
            {
                return data[index];
            }
            set
            {
                data[index] = value;
            }
        }
        private List<Datum> data { get; set; }

        public Data()
        {
            data = new List<Datum>();
        }
        public Data(List<Datum> data)
        {
            this.data = data;
        }

        public Data eachIndex(Action<int> func, Func<int, bool> terminate)
        {
            for(int i = 0; i < Count; i++)
            {
                func(i);
                if (terminate(i)) return this;
            }
            return this;
        }
        public Data eachIndex(Action<int> func)
        {
             return eachIndex(
                func, 
                i => false);
        }

        public Data each(Action<Datum> func, Func<Datum, bool> terminate)
        {
            return eachIndex(
                i => func(this[i]), 
                i => terminate(this[i]));
        }
        public Data each(Action<Datum> func)
        {
            return each(
                func,
                i => false);
        }

        public Data select(Func<Datum, Datum> func)
        {
            return new Data(
                data.Select(func)
                .ToList());
        }
        public List<T> selectList<T>(Func<Datum,T> func)
        {
            return data.Select(func).ToList();
        }

        public Data where(Func<Datum, bool> func)
        {
            return new Data(
                data.Where(func)
                .ToList());
        }
        public Data Add(Datum item)
        {
            data.Add(item);
            return this;
        }
        public Data Remove(Datum item)
        {
            data.Remove(item);
            return this;
        }

        public static Data FromFile(string filepath, int classDim = 0)
        {
            Data ret = new Data();
            using (FileStream fileStream = new FileStream(filepath, FileMode.Open, FileAccess.Read))
            {
                using (var streamReader = new StreamReader(fileStream))
                {
                    string line;
                    while ((line = streamReader.ReadLine()) != null)
                    {
                        var arr = line.Replace(" ", "").Split(',','\t').ToList();
                        Vector datumClass = null;
                        if (classDim != 0)
                        {
                            datumClass = new Vector(arr.PopLast(classDim).Select(i => double.Parse(i)).ToArray());
                        }
                        Vector datum = new Vector(arr.Select(i => double.Parse(i)).ToArray());
                        ret.Add(new Datum(datum, datumClass));
                    }
                }
            }
            return ret;
        }
        public static Data FromResource(string filepath, int classDim = 0)
        {
            return FromFile(ToResourcePath(filepath), classDim);
        }

        public static string ToResourcePath(string filepath)
        {
            return @"..\..\Resources\" + filepath;
        }
    }
}
