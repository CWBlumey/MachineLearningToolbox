﻿namespace MachineLearningToolbox.Core
{
    public class Datum
    {
        public Vector Value { get; set; }
        public Vector Data { get; set; }
        public object Context { get; set; }

        public Datum(Vector data, Vector value, object context)
        {
            Value = value;
            Data = data;
            Context = context;
        }
        public Datum(Vector data, Vector value) : this(data, value, null) { }
        public Datum(Vector data) : this(data, default(Vector)) { }
    }

}
