﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MachineLearningToolbox.Core.KingsCross
{
    public class KCMacro : IKCMacro
    {
        public int CurrentIteration
        {
            get
            {
                return this._currentIteration;
            }
        }

        public int CurrentLoop
        {
            get
            {
                return this._currentLoop;
            }
        }

        public double EndingValue
        {
            get
            {
                return this._endingValue;
            }
        }

        public bool HasCompleted
        {
            get
            {
                return this._hasCompleted;
            }
        }

        public double StartingValue
        {
            get
            {
                return this._startingValue;
            }
        }

        public double StepAmount
        {
            get
            {
                return this._stepAmount;
            }
        }

        public double Value
        {
            get
            {
                return this._value;
            }
        }

        public bool WillLoop
        {
            get
            {
                return this._willLoop;
            }
        }


        private int     _currentIteration;
        private int     _currentLoop;
        private double  _endingValue;
        private bool    _hasCompleted;
        private double  _startingValue;
        private double  _stepAmount;
        private double  _value;
        private bool    _willLoop;



        public KCMacro(double startingValue = 0.0, double endingValue = 100.0, double stepAmount = 1.0, bool allowLooping = false)
        {
            this._currentIteration  = 0;
            this._currentLoop       = 0;
            this._endingValue       = endingValue;
            this._hasCompleted      = false;
            this._startingValue     = startingValue;
            this._stepAmount        = stepAmount;
            this._willLoop          = allowLooping;
            this._value             = this._startingValue - this._stepAmount;
        }
        

        public double NextValue()
        {
            if (this._hasCompleted) return this._value;

            this._currentIteration++;

            double nextValue = this._value += this._stepAmount;

            if (nextValue >= this._endingValue)
            {
                if (this._willLoop)
                    nextValue = this._startingValue + (this._endingValue - this._value);
                else
                {
                    this._currentLoop++;

                    this._hasCompleted = true;
                    nextValue = this._endingValue;
                }
            }

            this._value = nextValue;

            return this._value;
        }
    }
}
