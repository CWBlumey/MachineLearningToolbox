﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MachineLearningToolbox.Core.KingsCross
{
    public class KCParameter : IKCValuedParameter
    {
        public bool IsValidValue
        {
            get
            {
                return this.TestValidation(this._value);
            }
        }

        public double Max
        {
            get
            {
                return this.Max;
            }
        }

        public double Min
        {
            get
            {
                return this.Min;
            }
        }

        public double Value
        {
            get
            {
                return this._value;
            }
        }

        private double _min;
        private double _max;
        private double _value;

        public KCParameter(double min, double max)
        {
            this._min = min;
            this._max = max;
            this._value = min;
        }

        public KCParameter(IKCParameter parameter) 
            : this(parameter.Min, parameter.Max)
        {

        }

        public KCParameter(IKCParameter parameter, double value)
            : this(parameter.Min, parameter.Max)
        {
            this._value = value;
        }

        protected virtual bool TestValidation(double value)
        {
            return (value < this.Max && value > this.Min);
        }

        public void SetValue(double value)
        {
            this._value = value;
        }
    }
}
