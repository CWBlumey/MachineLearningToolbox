﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MachineLearningToolbox.Core.KingsCross
{
    public interface IKCEngineer<TSystem, TData>  
        where TSystem : IKCSystem<TData>
        where TData : IKCDataItem
    {
        IKCCollection<IKCParameterAutomator> Automators { get; }
        IKCCollection<TSystem> Create(int amountOfSystems); 
    }
}
