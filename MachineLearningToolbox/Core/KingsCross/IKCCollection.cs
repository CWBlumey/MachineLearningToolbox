﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MachineLearningToolbox.Core.KingsCross
{
    public interface IKCCollection
    {
        bool IsReadOnly { get; }
        int Count { get; }
    }

    public interface IKCCollection<T> : IEnumerable<T>, IKCCollection
    {
        T this[int index] { get; }
    }
}
