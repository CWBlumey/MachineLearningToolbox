﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MachineLearningToolbox.Core.KingsCross
{
    public interface IKCDataSet<T>
        where T : IKCDataItem
    {
        int DataCount { get; }
        IKCCollection<T> TrainingData { get; }
        IKCCollection<T> TestingData { get; }
        IKCCollection<T> ValidationData { get; }
    }

    
}
