﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MachineLearningToolbox.Core.KingsCross
{
    public interface IKCParameterAutomator
    {
        IKCMacro Macro { get; }
        IKCParameter Parameter { get; }
        IKCValuedParameter CurrentParameterValue { get; }
    }
}
