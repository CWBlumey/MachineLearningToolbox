﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MachineLearningToolbox.Core.KingsCross
{
    public class KCCollection<T> : IKCCollection<T>
    {
        public T this[int index]
        {
            get
            {
                return this._internalList[index];
            }
        }

        public int Count
        {
            get
            {
                return this._internalList.Count;
            }
        }

        public bool IsReadOnly
        {
            get
            {
                return false;
            }
        }

        private List<T> _internalList;

        public KCCollection()
        {
            this._internalList = new List<T>();
        }

        public KCCollection(List<T> data)
        {
            this._internalList = data;
        }

        public void Add(T item)
        {
            this._internalList.Add(item);
        }

        public IEnumerator<T> GetEnumerator()
        {
            return this._internalList.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return this.GetEnumerator();
        }
    }
}
