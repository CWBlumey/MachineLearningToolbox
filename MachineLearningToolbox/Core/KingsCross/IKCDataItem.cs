﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MachineLearningToolbox.Core.KingsCross
{
    public interface IKCDataItem
    {
        int Key { get; }
        double NeuralValue { get; }
    }

    public interface IKCDataItem<T> : IKCDataItem
    {
        T Value { get; }
    }
}
