﻿namespace MachineLearningToolbox.Core.KingsCross
{
    public interface IKCPlan<TSystem, TData>
        where TSystem : IKCSystem<TData>
        where TData : IKCDataItem
    {
        TSystem                         BestSystem { get; }
        IKCEngineer<TSystem, TData>     Engineer { get; }

        void                            Execute(int phases);
    }
}