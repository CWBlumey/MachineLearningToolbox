﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MachineLearningToolbox.Core.KingsCross
{
    public interface IKCMacro
    {
        double Value { get; }
        double StepAmount { get; }
        double StartingValue { get; }
        double EndingValue { get; }
        bool WillLoop { get; }
        int CurrentIteration { get; }
        int CurrentLoop { get; }
        bool HasCompleted { get; }

        double NextValue();
    }
}
