﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MachineLearningToolbox.Core.KingsCross
{
    public class KCEngineer<TSystem, TData> : IKCEngineer<TSystem, TData>
        where TSystem : IKCSystem<TData>
        where TData : IKCDataItem
    {
        public IKCCollection<IKCParameterAutomator> Automators
        {
            get
            {
                return this._automators;
            }
        }

        private KCCollection<IKCParameterAutomator> _automators;

        public KCEngineer()
        {
            this._automators = new KCCollection<IKCParameterAutomator>();
        }

        public IKCCollection<TSystem> Create(int amountOfSystems)
        {
            throw new NotImplementedException();
        }
    }
}
