﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MachineLearningToolbox.Core.KingsCross
{
    public class KCParameterAutomator : IKCParameterAutomator
    {

        public IKCMacro Macro
        {
            get
            {
                return this._macro;
            }
        }

        public IKCParameter Parameter
        {
            get
            {
                return this._parameter;
            }
        }

        public IKCValuedParameter CurrentParameterValue
        {
            get
            {
                return new KCParameter(this._parameter, this.GetValue());
            }
        }

        private IKCMacro            _macro;
        private IKCValuedParameter  _parameter;

        public KCParameterAutomator(IKCMacro macro, IKCParameter parameter)
        {
            this._macro     = macro;
            this._parameter = new KCParameter(parameter);
        }

        protected virtual double GetValue()
        {
            return this._macro.Value;
        }
        

    }
}
