﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MachineLearningToolbox.Core.KingsCross
{
    public interface IKCSystem<TIn>
        where TIn : IKCDataItem
    {
        IKCCollection<IKCParameter> Parameters { get; }

    }
}
