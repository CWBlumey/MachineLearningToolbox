﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MachineLearningToolbox.Core.KingsCross
{
    public interface IKCScore
    {
        IKCCollection<IKCScore> SubScores { get; }
        double SuccessPercentage { get; }
        double Marks { get; }
        double MaximumScore { get; }
        IKCTest Test { get; }
        bool IsBetterScoreThan(IKCScore scoring);
        IKCScore Accumilate(IKCScore score);
    }
}
