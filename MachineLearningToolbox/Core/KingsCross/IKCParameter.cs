﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MachineLearningToolbox.Core.KingsCross
{
    public interface IKCParameter
    {
        double Min { get; }
        double Max { get; }
        bool IsValidValue { get; }
    }

    public interface IKCValuedParameter : IKCParameter
    {
        double Value { get; }
        void SetValue(double value);
    }
}
